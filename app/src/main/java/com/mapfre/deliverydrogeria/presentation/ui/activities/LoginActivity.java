/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.ui.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.mapfre.deliverydrogeria.R;
import com.mapfre.deliverydrogeria.domain.executor.impl.ThreadExecutor;
import com.mapfre.deliverydrogeria.domain.models.LoginModel;
import com.mapfre.deliverydrogeria.presentation.presenters.LoginPresenter;
import com.mapfre.deliverydrogeria.presentation.presenters.impl.LoginPresenterImpl;
import com.mapfre.deliverydrogeria.threading.MainThreadImpl;
import com.mapfre.deliverydrogeria.utilities.AppPrefs;
import com.mapfre.deliverydrogeria.utilities.SessionManager;

/**
 * Created by Bardur Thomsen on 09/09/2017.
 *
 * Activity which enables the user to sign in to the application
 */

public class LoginActivity extends BaseActivity implements LoginPresenter.View
{

    private ProgressDialog  progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.login_dialog_message));

        Button loginButton = (Button) findViewById(R.id.btn_login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        CheckBox rememberMe = (CheckBox) findViewById(R.id.cb_remember_me);
        rememberMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                AppPrefs.getInstance().saveBoolean(AppPrefs.REMEMBER_ME, b);
            }
        });
    }

    @Override
    public void loginSuccess(String userCode)
    {
        hideProgress();
        SessionManager.getInstance().saveUser(userCode);
        startActivity(new Intent(LoginActivity.this, PedidoActivity.class));
    }

    @Override
    public void loginFail(String message)
    {
        hideProgress();
        loginErrorDialog(message);
    }

    @Override
    public void unauth()
    {
        hideProgress();
    }

    @Override
    public void error() {
        hideProgress();
        unkownErrorDialog();
    }

    @Override
    public void showProgress()
    {
        if(progressDialog != null){
            if(!progressDialog.isShowing()){
                progressDialog.show();
            }
        }
    }

    @Override
    public void hideProgress()
    {
        if(progressDialog != null){
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }
    }

    private void login()
    {
        String user = ((EditText) findViewById(R.id.et_user)).getText().toString();
        String password = ((EditText)findViewById(R.id.et_password)).getText().toString();

        if(user.isEmpty() || password.isEmpty()){
            Toast.makeText(LoginActivity.this, getString(R.string.empty_fields_warning), Toast.LENGTH_LONG).show();
        }else {
            login(user,password);
        }
    }

    private void login(String user, String password)
    {
        showProgress();

        LoginModel model = new LoginModel();

        model.setUserName(user);
        model.setPassword(password);

        LoginPresenter presenter = new LoginPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this
        );

        presenter.login(model);
    }

    private void loginErrorDialog(String message)
    {
        if(message.isEmpty()){
            message = getString(R.string.login_error_message);
        }

        new AlertDialog.Builder(this)
                .setTitle(R.string.error)
                .setMessage(message)
                .setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void unkownErrorDialog()
    {

        new AlertDialog.Builder(this)
                .setTitle(R.string.error)
                .setMessage(R.string.some_error_occurred)
                .setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}
