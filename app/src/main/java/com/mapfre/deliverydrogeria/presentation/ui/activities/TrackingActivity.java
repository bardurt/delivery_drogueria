/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.ui.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mapfre.deliverydrogeria.R;
import com.mapfre.deliverydrogeria.domain.executor.impl.ThreadExecutor;
import com.mapfre.deliverydrogeria.domain.models.PedidoStateModel;
import com.mapfre.deliverydrogeria.domain.models.TrackingModel;
import com.mapfre.deliverydrogeria.domain.models.TrackingTime;
import com.mapfre.deliverydrogeria.presentation.presenters.PedidoStatePresenter;
import com.mapfre.deliverydrogeria.presentation.presenters.TrackingPresenter;
import com.mapfre.deliverydrogeria.presentation.presenters.impl.PedidoStatePresenterImpl;
import com.mapfre.deliverydrogeria.presentation.presenters.impl.TrackingPresenterImpl;
import com.mapfre.deliverydrogeria.presentation.ui.adapters.TimeAdapter;
import com.mapfre.deliverydrogeria.threading.MainThreadImpl;
import com.mapfre.deliverydrogeria.utilities.Constants;
import com.mapfre.deliverydrogeria.utilities.SessionManager;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Activity to send tracking updates to the web server
 *
 * @author Bardur Thomsen
 * @version 1.0 23/09/2017.
 */
public class TrackingActivity extends BaseActivity implements
        LocationListener,
        TrackingPresenter.View,
        PedidoStatePresenter.View,
        OnMapReadyCallback {

    private MapView mapView;
    private GoogleMap map;

    // Time between tracking updates
    private long                    TRACKING_UPDATE_TIME = 1000 * 60 * 2;  //  2 minutes

    // The minimum distance to change Updates in meters
    private static final long       MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 METERS

    // The minimum time between updates in milliseconds
    private static final long       MIN_UPDATE_TIME = 1000 * 20; // 20 seconds

    private static final int        MINUTE = 1000 * 60;

    // Is tracking activated or not
    private boolean                 isTracking = false;

    // location manager to get location update
    private LocationManager         locationManager;

    // Most accurate location which we have
    private Location                currentBestLocation;

    // TextView to show if tracking is on or off
    private TextView                trackingStateTextView;

    // Button to start and stop tracking
    private Button                  trackingButton;

    // Dialog to notify the user that gps is disabled
    private Dialog                  gpsDialog;

    // Format the date
    private SimpleDateFormat        dateFormat;

    // Format decimals
    private DecimalFormat           decimalFormat;

    // Class to send location updates in the background
    private LocationTracker         locationTracker;

    // Dialog for settings, changing the update time
    private Dialog                  settingsDialog;

    // List of time for updates
    private ArrayList<TrackingTime> trackingTimeArrayList;

    // Adapter to populate ListView with TrackingTime
    private TimeAdapter             timeAdapter;

    // Presenter to send tracking updates
    private TrackingPresenter       trackingPresenter;

    // Presenter to change state of a Pedido
    private PedidoStatePresenter    pedidoStatePresenter;

    private String user;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);


        trackingStateTextView = findViewById(R.id.tv_tracking_state);

        trackingButton = findViewById(R.id.btn_tracking);

        // If state of Pedido is Assigned, then we can start a delivery
        // otherwise we have to finish the delivery
        if(selectedPedido.getESTADO().equalsIgnoreCase(Constants.MapfreStates.STATE_ASSIGNED)) {
            trackingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isTracking) {
                        setPedidoStateToG();
                    } else {
                        stopTracking();
                    }
                }
            });
        } else if(selectedPedido.getESTADO().equalsIgnoreCase("en entrega")){
            trackingButton.setText(R.string.finish_delivery);
            trackingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stopTracking();
                }
            });
        }

        setGpsDialog();
        getLocation();

        if(isGpsOn()){
            notifyGpsDisabled();
        }

        trackingPresenter = new TrackingPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this
        );

        dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        decimalFormat = new DecimalFormat("#.00000000");

        settingsDialog = new Dialog(TrackingActivity.this);
        settingsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(R.layout.dialog_tracking_time);
        settingsDialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT);

        fillTime();

        timeAdapter = new TimeAdapter(TrackingActivity.this, R.layout.list_item_tracking_time, trackingTimeArrayList);

        ListView timeListView = settingsDialog.findViewById(R.id.lv_tracking_time);
        timeListView.setAdapter(timeAdapter);

        timeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectTimeAt(i);
                TRACKING_UPDATE_TIME = trackingTimeArrayList.get(i).getSeconds();
                timeAdapter.notifyDataSetChanged();
                settingsDialog.dismiss();
            }
        });

        timeAdapter.notifyDataSetChanged();

        pedidoStatePresenter = new PedidoStatePresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this);


        user = SessionManager.getInstance().getUser();

        progressBar = findViewById(R.id.pb_tracking);

        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(this);
    }

    /** Method to create a GPS warning dialog **/
    private void setGpsDialog()
    {
        gpsDialog = new Dialog(TrackingActivity.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        gpsDialog.setContentView(R.layout.dialog_gps_warning);
        gpsDialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT);
        gpsDialog.setCancelable(false);
        gpsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Button gpsButton = gpsDialog.findViewById(R.id.btn_activate_gps);

        gpsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gpsDialog.dismiss();
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });

        Button quitButton = gpsDialog.findViewById(R.id.btn_quit);
        quitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishAffinity();
            }
        });
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        /* Make sure to stop location updates when they are not needed*/
        stopUsingGPS();
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tracking, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings:
                settingsDialog.show();
                return true;
            default:
                return onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        getLocation();

        /* Check if GPS is enabled*/
        try{
            if(!isGpsOn()){
                notifyGpsDisabled();
            }else {
                notifyGpsEnabled();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void startTracking()
    {
        startNotification();
        progressBar.setVisibility(View.VISIBLE);

        trackingButton.setText(getString(R.string.finish_delivery));
        trackingStateTextView.setText(getString(R.string.tracking_activated));

        findViewById(R.id.ll_tracking_info).setVisibility(View.VISIBLE);

        synchronized (this){
            isTracking = true;
        }

        locationTracker = new LocationTracker();
        locationTracker.start();
    }

    private void stopTracking()
    {
        stopNotification();
        progressBar.setVisibility(View.INVISIBLE);

        trackingButton.setText(getString(R.string.start_delivery));

        trackingStateTextView.setText(getString(R.string.tracking_deactivated));

        findViewById(R.id.ll_tracking_info).setVisibility(View.GONE);

        synchronized (this){
            isTracking = false;
        }

        if(locationTracker != null) {
            locationTracker.stopTracking();
        }

        /* We are finished, set the state of the pedido to delivered, code C */
        setPedidoStateToC();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle)
    {

    }

    @Override
    public void onProviderEnabled(String s)
    {
        /* Notify the user if GPS has been enable **/
        if(s.equals(LocationManager.GPS_PROVIDER)){
            notifyGpsEnabled();
        }
    }

    @Override
    public void onProviderDisabled(String s)
    {
        /* Notify the user if GPS has been disabled **/
        if(s.equals(LocationManager.GPS_PROVIDER)){
            notifyGpsDisabled();
        }
    }

    private void notifyGpsDisabled()
    {
        if(gpsDialog != null){
            if(!gpsDialog.isShowing()){
                gpsDialog.show();
            }
        }
    }

    private void notifyGpsEnabled()
    {
        if(gpsDialog != null){
            if(gpsDialog.isShowing()){
                gpsDialog.dismiss();
            }
        }
    }

    private void getLocation()
    {
        try {

            locationManager = (LocationManager)
                    this.getSystemService(Context.LOCATION_SERVICE);

            // getting GPS status
            boolean gpsOn = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            boolean isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            if (gpsOn || isNetworkEnabled) {
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_UPDATE_TIME,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                }
                // if GPS Enabled get lat/long using GPS Services
                if (gpsOn) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_UPDATE_TIME,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                }
            }

        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void stopUsingGPS()
    {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    private double getLatitude()
    {
        double latitude = 0.0d;

        if(getLastBestLocation() != null){
            latitude = getLastBestLocation().getLatitude();
        }

        return latitude;
    }

    private double getLongitude()
    {
        double longitude = 0.0d;

        if(getLastBestLocation() != null){
            longitude = getLastBestLocation().getLongitude();
        }

        return longitude;
    }

    private double getAltitude()
    {
        double altitude = 0.0d;

        if(getLastBestLocation() != null){
            altitude = getLastBestLocation().getAltitude();
        }

        return altitude;
    }

    @Override
    public void onLocationChanged(Location location)
    {
        makeUseOfNewLocation(location);

        if(currentBestLocation == null){
            currentBestLocation = location;
        }
    }

    private synchronized Location getLastBestLocation()
    {
        try {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            long GPSLocationTime = 0;

            if (null != locationGPS) {
                GPSLocationTime = locationGPS.getTime();
            }

            long NetLocationTime = 0;

            if (null != locationNet) {
                NetLocationTime = locationNet.getTime();
            }

            if ( 0 < GPSLocationTime - NetLocationTime ) {
                return locationGPS;
            } else {
                return locationNet;
            }
        }catch (SecurityException e){
            e.printStackTrace();
        }

        return null;
    }

    private void makeUseOfNewLocation(Location location)
    {
        if ( isBetterLocation(location, currentBestLocation) ) {
            currentBestLocation = location;
        }
    }

    private boolean isBetterLocation(Location location, Location currentBestLocation)
    {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > MINUTE;
        boolean isSignificantlyOlder = timeDelta < -MINUTE;
        boolean isNewer = timeDelta > 0;

        // If it's been more than @Minute since the current location, use the new location,
        // because the appUser has likely moved.
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse.
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /* Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2)
    {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private boolean isGpsOn()
    {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @Override
    public void success(TrackingModel model)
    {
        updateTrackingInfo(model);
    }

    @Override
    public void error()
    {

    }

    @Override
    public void unauth()
    {

    }

    @Override
    public void showProgress()
    {

    }

    @Override
    public void hideProgress()
    {

    }

    /** send location update to server **/
    private void sendTracking()
    {
        TrackingModel model = new TrackingModel();
        model.setAltitude(getAltitude());
        model.setLatitude(getLatitude());
        model.setLongitude(getLongitude());
        model.setTime(dateFormat.format(Calendar.getInstance().getTime()));
        model.setCucrgstro(user);
        model.setNrqrmnto(selectedPedido.getNRQRMTO());

        trackingPresenter.sendTracking(model);
    }

    private void updateTrackingInfo(final TrackingModel model)
    {

        LatLng latLng = new LatLng(model.getLatitude(), model.getLongitude());

        map.addMarker(new MarkerOptions().position(latLng)
                .title("")
        .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("point",12,12))));

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f));

        mapView.onResume();

    }

    @Override
    public void onBackPressed()
    {
        stopTracking();
        startActivity(new Intent(TrackingActivity.this, PedidoActivity.class));
    }

    @Override
    public void onStateChanged(String stateCode) {

        if(stateCode.equalsIgnoreCase("G")){
            startTracking();
        } else {
            trackingButton.setEnabled(false);
            trackingStateTextView.setText(R.string.entrega_finalizada);
        }
    }

    @Override
    public void onStateError(String stateCode, String message) {
        Toast.makeText(TrackingActivity.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGeneralError() {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {

            map = googleMap;
            map.getUiSettings().setMyLocationButtonEnabled(false);
            map.setMyLocationEnabled(true);

            LatLng latLng = new LatLng(0d, 0d);

            try {
                latLng = new LatLng(getLastBestLocation().getLatitude(), getLastBestLocation().getLongitude());
            } catch (NullPointerException e){
                e.printStackTrace();
            }

            if(currentBestLocation != null){
                latLng = new LatLng(currentBestLocation.getLatitude(), currentBestLocation.getLongitude());
            }

            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f));

            mapView.onResume();

        }catch (SecurityException e){
            e.printStackTrace();
        }

    }

    /** Inner Class to handle tracking in the background **/
    private class LocationTracker extends Thread
    {
        private volatile boolean running = true;

        @Override
        public void run()
        {
            while (running) {

                sendTracking();
                try {
                    Thread.sleep(TRACKING_UPDATE_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        void stopTracking()
        {
            running = false;
        }
    }

    /** Set up available update times **/
    private void fillTime(){

        trackingTimeArrayList = new ArrayList<>();

        TrackingTime t1 = new TrackingTime();
        t1.setSeconds(20000); // 30 seconds
        t1.setTimeString("20 " + getString(R.string.seconds));

        TrackingTime t2 = new TrackingTime();
        t2.setSeconds(30000); // 30 seconds
        t2.setTimeString("30 " + getString(R.string.seconds));

        TrackingTime t3 = new TrackingTime();
        t3.setSeconds(60000); // 1 minute
        t3.setTimeString("60 " + getString(R.string.seconds));

        TrackingTime t4 = new TrackingTime();
        t4.setSeconds(120000); // 2 minutes
        t4.setTimeString("2 " + getString(R.string.minutes));
        t4.setSelected(true);

        trackingTimeArrayList.add(t1);
        trackingTimeArrayList.add(t2);
        trackingTimeArrayList.add(t3);
        trackingTimeArrayList.add(t4);
    }

    /** Method to select a specific update time **/
    private void selectTimeAt(int index)
    {
        for(int i = 0; i < trackingTimeArrayList.size(); i++){
            trackingTimeArrayList.get(i).setSelected(false);
        }

        trackingTimeArrayList.get(index).setSelected(true);
    }

    private void setPedidoStateToG(){

        PedidoStateModel stateModel = new PedidoStateModel();
        stateModel.setNrqrmnto(selectedPedido.getNRQRMTO());
        stateModel.setSrqrmnnto("G");

        pedidoStatePresenter.changeState(stateModel);
    }

    private void setPedidoStateToC(){

        PedidoStateModel stateModel = new PedidoStateModel();
        stateModel.setNrqrmnto(selectedPedido.getNRQRMTO());
        stateModel.setSrqrmnnto("C");

        pedidoStatePresenter.changeState(stateModel);
    }

    public Bitmap resizeMapIcons(String iconName, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier(iconName, "drawable", getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }
}
