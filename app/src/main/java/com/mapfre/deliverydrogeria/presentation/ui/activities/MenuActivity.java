/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.ui.activities;

import android.widget.Toast;

import com.mapfre.deliverydrogeria.R;

/**
 * TODO define class.
 *
 * @author Bardur Thomsen
 * @version 1.0 06/12/2017.
 */
public class MenuActivity extends BaseActivity {

    private int backCount = 0;

    @Override
    public void onBackPressed() {

        backCount++;

        if(backCount > 1){

            finishAffinity();

        } else {

            Toast.makeText(MenuActivity.this, R.string.press_back_to_quit, Toast.LENGTH_SHORT).show();

        }

        countBackTime();
    }

    private void exit(){
        finishAffinity();
    }

    private void countBackTime(){

        Runnable r = new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    synchronized (this) {
                        backCount = 0;
                    }
                }
            }
        };

        Thread t = new Thread(r);

        t.start();
    }

}
