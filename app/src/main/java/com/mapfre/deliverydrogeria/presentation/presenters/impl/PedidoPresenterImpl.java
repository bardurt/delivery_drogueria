package com.mapfre.deliverydrogeria.presentation.presenters.impl;

import com.mapfre.deliverydrogeria.domain.executor.Executor;
import com.mapfre.deliverydrogeria.domain.executor.MainThread;
import com.mapfre.deliverydrogeria.domain.executor.impl.ThreadExecutor;
import com.mapfre.deliverydrogeria.domain.interactors.PedidoInteractor;
import com.mapfre.deliverydrogeria.domain.interactors.impl.PedidoInteractorImpl;
import com.mapfre.deliverydrogeria.domain.models.PedidoModel;
import com.mapfre.deliverydrogeria.domain.models.UserModel;
import com.mapfre.deliverydrogeria.presentation.presenters.PedidoPresenter;
import com.mapfre.deliverydrogeria.presentation.presenters.base.AbstractPresenter;
import com.mapfre.deliverydrogeria.threading.MainThreadImpl;

import java.util.List;

/**
 * Created by Bardur Thomsen on 07/09/2017.
 */

public class PedidoPresenterImpl extends AbstractPresenter implements PedidoPresenter, PedidoInteractor.Callback
{

    private final View view;

    public PedidoPresenterImpl(Executor executor, MainThread mainThread, View view)
    {
        super(executor, mainThread);

        this.view = view;
    }

    @Override
    public void pedidosDownloaded(List<PedidoModel> pedidoList)
    {
        view.pedidosDownloaded(pedidoList);
    }

    @Override
    public void error()
    {
        view.error();
    }

    @Override
    public void unauth()
    {
        view.unauth();
    }

    @Override
    public void resume()
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void stop()
    {

    }

    @Override
    public void destroy()
    {

    }

    @Override
    public void downloadPedidos(UserModel model)
    {
        PedidoInteractor interactor = new PedidoInteractorImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                model
        );

        interactor.execute();
    }
}
