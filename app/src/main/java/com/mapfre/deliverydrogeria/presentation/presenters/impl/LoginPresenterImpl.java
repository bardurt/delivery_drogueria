/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.presenters.impl;

import com.mapfre.deliverydrogeria.domain.executor.Executor;
import com.mapfre.deliverydrogeria.domain.executor.MainThread;
import com.mapfre.deliverydrogeria.domain.executor.impl.ThreadExecutor;
import com.mapfre.deliverydrogeria.domain.interactors.LoginInteractor;
import com.mapfre.deliverydrogeria.domain.interactors.impl.LoginInteractorImpl;
import com.mapfre.deliverydrogeria.domain.models.LoginModel;
import com.mapfre.deliverydrogeria.presentation.presenters.LoginPresenter;
import com.mapfre.deliverydrogeria.presentation.presenters.base.AbstractPresenter;
import com.mapfre.deliverydrogeria.threading.MainThreadImpl;

/**
 * Created by Bardur Thomsen on 07/09/2017.
 */

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, LoginInteractor.Callback
{

    private final View view;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, View view)
    {
        super(executor, mainThread);

        this.view = view;
    }

    @Override
    public void resume()
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void stop()
    {

    }

    @Override
    public void destroy()
    {

    }

    @Override
    public void loginSuccess(String userCode)
    {
        view.loginSuccess(userCode);
    }

    @Override
    public void loginFail(String message)
    {
        view.loginFail(message);
    }

    @Override
    public void unauth()
    {
        view.unauth();
    }

    @Override
    public void error() {
        view.error();
    }

    @Override
    public void login(LoginModel model)
    {
        LoginInteractor interactor = new LoginInteractorImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                model
        );

        interactor.execute();
    }
}
