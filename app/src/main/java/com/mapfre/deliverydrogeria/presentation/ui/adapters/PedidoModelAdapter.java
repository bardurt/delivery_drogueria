/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mapfre.deliverydrogeria.R;
import com.mapfre.deliverydrogeria.domain.models.PedidoModel;

import java.util.List;

/**
 * Created by Bardur Thomsen on 15/09/2017.
 *
 * Adapter class to populate a ListView with PedidoModels
 */

public class PedidoModelAdapter extends ArrayAdapter<PedidoModel>
{

    public PedidoModelAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<PedidoModel> objects)
    {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        PedidoModelWrapper wrapper;

        if (convertView==null){
            convertView = ((Activity)getContext()).getLayoutInflater().inflate(R.layout.list_item_pedidomodel, null);
            wrapper = new PedidoModelWrapper(convertView);
            convertView.setTag(wrapper);
        }
        else{
            wrapper = (PedidoModelWrapper) convertView.getTag();
        }

        wrapper.populateFrom(getItem(position));

        return convertView;
    }

    private class PedidoModelWrapper
    {
        private final View  row;
        private TextView    idView;
        private TextView    stateView;
        private TextView    clientView;
        private TextView    dateView;

        public PedidoModelWrapper(View view)
        {
            this.row = view;
        }

        private TextView getIdView()
        {
            if(idView == null){
                idView = row.findViewById(R.id.tv_pedido_id);
            }

            return idView;
        }


        private TextView getStateView()
        {
            if(stateView == null){
                stateView = row.findViewById(R.id.tv_pedido_state);
            }

            return stateView;
        }

        private TextView getClientView()
        {
            if(clientView == null){
                clientView = row.findViewById(R.id.tv_pedido_client);
            }

            return clientView;
        }

        private TextView getDateView(){
            if(dateView == null){
                dateView = row.findViewById(R.id.tv_pedido_date);
            }

            return dateView;
        }

        /**
         * Method to add model into ListView item
         * @param pedidoModel - Model containing the parameters for item
         */
        public void populateFrom(final PedidoModel pedidoModel)
        {
            getIdView().setText(pedidoModel.getNRQRMTO());
            getStateView().setText(getContext().getString(R.string.state) + ": " + pedidoModel.getESTADO());
            getClientView().setText(getContext().getString(R.string.client) + ": " + pedidoModel.getCLIENTE());
            getDateView().setText(getContext().getString(R.string.date) + ": " + pedidoModel.getFRQRMTO());

        }
    }
}