/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.presenters.impl;

import com.mapfre.deliverydrogeria.domain.executor.Executor;
import com.mapfre.deliverydrogeria.domain.executor.MainThread;
import com.mapfre.deliverydrogeria.domain.executor.impl.ThreadExecutor;
import com.mapfre.deliverydrogeria.domain.interactors.TrackingInteractor;
import com.mapfre.deliverydrogeria.domain.interactors.impl.TrackingInteractorImpl;
import com.mapfre.deliverydrogeria.domain.models.TrackingModel;
import com.mapfre.deliverydrogeria.presentation.presenters.TrackingPresenter;
import com.mapfre.deliverydrogeria.presentation.presenters.base.AbstractPresenter;
import com.mapfre.deliverydrogeria.threading.MainThreadImpl;

/**
 * Created by Bardur Thomsen on 12/09/2017.
 */

public class TrackingPresenterImpl extends AbstractPresenter implements TrackingPresenter, TrackingInteractor.Callback
{
    private final View view;

    public TrackingPresenterImpl(Executor executor, MainThread mainThread, View view)
    {
        super(executor, mainThread);

        this.view = view;
    }

    @Override
    public void resume()
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void stop()
    {

    }

    @Override
    public void destroy()
    {

    }

    @Override
    public void success(TrackingModel model)
    {
        view.success(model);
    }

    @Override
    public void error()
    {
        view.error();
    }

    @Override
    public void unauth()
    {
        view.unauth();
    }

    @Override
    public void sendTracking(TrackingModel model)
    {
        TrackingInteractor interactor = new TrackingInteractorImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                model
        );

        interactor.execute();
    }
}
