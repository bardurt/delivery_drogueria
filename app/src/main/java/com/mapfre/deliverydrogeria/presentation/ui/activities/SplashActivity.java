/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.ui.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.mapfre.deliverydrogeria.R;
import com.mapfre.deliverydrogeria.utilities.AppPrefs;
import com.mapfre.deliverydrogeria.utilities.SessionManager;

/**
 * Created by Bardur Thomsen on 09/09/2017.
 *
 * Main activity for the application. This is the entry point of the Application
 */

public class SplashActivity extends AppCompatActivity
{
    private static final long SPLASH_TIME = 1000; // 1 second;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash);

        try {

            if (getSupportActionBar() != null) {
                getSupportActionBar().hide();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        setVersionCode();

        Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.unblend);

        findViewById(R.id.iv_logo).startAnimation(slideDown);

        SessionManager.getInstance().init(SplashActivity.this);
        AppPrefs.getInstance().init(SplashActivity.this);

        Runnable r = new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(SPLASH_TIME);
                }catch (Exception e){
                    e.printStackTrace();
                }finally {

                    if(AppPrefs.getInstance().getBoolean(AppPrefs.REMEMBER_ME)
                            && !SessionManager.getInstance().getUser().isEmpty()){

                        startActivity(new Intent(SplashActivity.this, PedidoActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    }
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
    }

    private void setVersionCode()
    {

        String version = "v. 1.0.0"; // default version code

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = "v. " + pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        ((TextView) findViewById(R.id.tv_version)).setText(version);
    }
}
