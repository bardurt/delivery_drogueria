package com.mapfre.deliverydrogeria.presentation.presenters.impl;

import com.mapfre.deliverydrogeria.domain.executor.Executor;
import com.mapfre.deliverydrogeria.domain.executor.MainThread;
import com.mapfre.deliverydrogeria.domain.executor.impl.ThreadExecutor;
import com.mapfre.deliverydrogeria.domain.interactors.PedidoStateInteractor;
import com.mapfre.deliverydrogeria.domain.interactors.impl.PedidoStateInteractorImpl;
import com.mapfre.deliverydrogeria.domain.models.PedidoStateModel;
import com.mapfre.deliverydrogeria.presentation.presenters.PedidoStatePresenter;
import com.mapfre.deliverydrogeria.presentation.presenters.base.AbstractPresenter;
import com.mapfre.deliverydrogeria.threading.MainThreadImpl;

/**
 * TODO define class.
 *
 * @author Bardur Thomsen
 * @version 1.0 23/10/2017.
 */
public class PedidoStatePresenterImpl extends AbstractPresenter implements PedidoStatePresenter, PedidoStateInteractor.Callback {

    private final View view;

    public PedidoStatePresenterImpl(Executor executor, MainThread mainThread, View view) {
        super(executor, mainThread);

        this.view = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onStateChanged(String stateCode) {
        view.onStateChanged(stateCode);
    }

    @Override
    public void onStateError(String stateCode, String message) {
        view.onStateError(stateCode, message);
    }

    @Override
    public void onGeneralError() {
        view.onGeneralError();
    }

    @Override
    public void changeState(PedidoStateModel stateModel) {
        PedidoStateInteractor interactor = new PedidoStateInteractorImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                stateModel
        );

        interactor.execute();
    }
}
