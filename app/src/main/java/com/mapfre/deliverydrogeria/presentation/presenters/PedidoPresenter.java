/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.presenters;

import com.mapfre.deliverydrogeria.domain.models.PedidoModel;
import com.mapfre.deliverydrogeria.domain.models.UserModel;
import com.mapfre.deliverydrogeria.presentation.presenters.base.BasePresenter;
import com.mapfre.deliverydrogeria.presentation.ui.BaseView;

import java.util.List;

/**
 * Created by Bardur Thomsen on 07/09/2017.
 */

public interface PedidoPresenter extends BasePresenter
{

    interface View extends BaseView
    {
        void pedidosDownloaded(List<PedidoModel> pedidoList);
        void error();
        void unauth();
    }

    /**
     * Method to download available Pedidos for a specific user.
     * This method starts the PedidoInteractor
     *
     * @param model - Model which contains parameters for downloading a list of Pedidos
     */
    void downloadPedidos(UserModel model);
}
