/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.ui.activities;

import android.Manifest;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.mapfre.deliverydrogeria.R;
import com.mapfre.deliverydrogeria.domain.models.PedidoModel;
import com.mapfre.deliverydrogeria.utilities.AndroidNetworkChecker;
import com.mapfre.deliverydrogeria.utilities.AppPrefs;
import com.mapfre.deliverydrogeria.utilities.NetworkChecker;
import com.mapfre.deliverydrogeria.utilities.SessionManager;

/**
 * Created by Bardur Thomsen on 09/09/2017.
 *
 * Base activity - this activity contains methods  and
 * variables which are common for all activities in the application.
 */

public class BaseActivity extends AppCompatActivity
{

    // Code to identify location permission
    private static final int            PERMISSION_LOCATION = 99;

    // NetworkChecker to detect network changes
    private NetworkChecker              networkChecker;

    // Is the application offline or not
    public static boolean               offline         = false;

    // The pedido which we have selected for delivery
    public static PedidoModel           selectedPedido;

    // id for notification.
    private final int                   mNotificationId = 1001;

    private NotificationManager         mNotifyMgr;
    private NotificationCompat.Builder  mBuilder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        networkChecker = new AndroidNetworkChecker(this);

        if(!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)){
            askLocationPermission();
        }

        mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.mapfre_logo)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.sending_position));

        mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause()
    {
        unregisterReceiver(networkStateReceiver);
        super.onPause();
    }

    private void getNetworkState(){

        offline = !networkChecker.hasMobileData() && !networkChecker.hasWifi();
    }


    /** broadcast receiver to listen for network changes **/
    private final BroadcastReceiver networkStateReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {

            networkChecker.refresh();

            if(!networkChecker.hasMobileData() && !networkChecker.hasWifi()){
                offline = true;
                notifyOffline();
            }else {
                offline = false;
            }
        }
    };

    /** Dialog to notify the user that the device is offline **/
    public void notifyOffline()
    {
        new AlertDialog.Builder(this)
                .setTitle(R.string.offline)
                .setMessage(R.string.offline_warning_message)
                .setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();

    }

    protected boolean hasPermission(String permission)
    {
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case PERMISSION_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission. ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                    }

                } else {
                    Toast.makeText(getApplicationContext(), R.string.gps_permission_message, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public boolean gpsAvailable()
    {
        return  ((hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)));
    }

    public void askLocationPermission()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION);
        }
    }

    public void startNotification()
    {
        mBuilder.setOngoing(true);
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    public void stopNotification()
    {
        mBuilder.setOngoing(false);
        mNotifyMgr.cancel(mNotificationId);
    }

    public void promptLogout(){
        new AlertDialog.Builder(this)
                .setTitle(R.string.attention)
                .setMessage(R.string.do_you_wish_to_close_your_session)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    /** Quit application and clear session data **/
    public void logout(){

        AppPrefs.getInstance().saveBoolean(AppPrefs.REMEMBER_ME,false);

        SessionManager.getInstance().destroySession();

        Intent i = new Intent(BaseActivity.this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(i);
    }

}
