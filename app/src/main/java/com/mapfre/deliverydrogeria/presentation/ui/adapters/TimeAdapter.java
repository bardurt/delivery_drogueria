/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.ui.adapters;

/**
 * Created by Bardur Thomsen on 15/09/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.mapfre.deliverydrogeria.R;
import com.mapfre.deliverydrogeria.domain.models.TrackingTime;
import com.mapfre.deliverydrogeria.presentation.ui.wrappers.TimeWrapper;

import java.util.List;

/** Listview adapter for TrackingTime **/
public class TimeAdapter extends ArrayAdapter<TrackingTime>
{

    public TimeAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<TrackingTime> objects)
    {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        TimeWrapper wrapper;

        if (convertView==null){
            convertView = ((Activity)getContext()).getLayoutInflater().inflate(R.layout.list_item_tracking_time, null);
            wrapper = new TimeWrapper(getContext(), convertView);
            convertView.setTag(wrapper);
        }
        else{
            wrapper = (TimeWrapper) convertView.getTag();
        }

        wrapper.populateFrom(getItem(position));

        return convertView;
    }
}