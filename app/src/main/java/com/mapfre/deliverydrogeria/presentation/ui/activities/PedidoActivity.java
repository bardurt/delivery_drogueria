/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mapfre.deliverydrogeria.R;
import com.mapfre.deliverydrogeria.domain.executor.impl.ThreadExecutor;
import com.mapfre.deliverydrogeria.domain.models.PedidoModel;
import com.mapfre.deliverydrogeria.domain.models.UserModel;
import com.mapfre.deliverydrogeria.presentation.presenters.PedidoPresenter;
import com.mapfre.deliverydrogeria.presentation.presenters.impl.PedidoPresenterImpl;
import com.mapfre.deliverydrogeria.presentation.ui.adapters.PedidoModelAdapter;
import com.mapfre.deliverydrogeria.threading.MainThreadImpl;
import com.mapfre.deliverydrogeria.utilities.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class PedidoActivity extends MenuActivity implements PedidoPresenter.View
{
    private ProgressDialog          progressDialog;
    private ArrayList<PedidoModel>  pedidoModels;
    private PedidoModelAdapter      pedidoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);

        pedidoModels = new ArrayList<>();

        pedidoAdapter = new PedidoModelAdapter(PedidoActivity.this,
                R.layout.list_item_pedidomodel,
                pedidoModels);

        ListView pedidoListView = findViewById(R.id.lv_pedidos);
        pedidoListView.setAdapter(pedidoAdapter);

        pedidoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPedido = pedidoModels.get(i);
                startActivity(new Intent(PedidoActivity.this, TrackingActivity.class));
            }
        });

        progressDialog = new ProgressDialog(PedidoActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.loading_orders_wait));

        downloadPedidos();
    }

    @Override
    public void showProgress()
    {
        if(progressDialog != null){
            if(!progressDialog.isShowing()){
                progressDialog.show();
            }
        }
    }

    @Override
    public void hideProgress()
    {
        if(progressDialog != null){
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public void pedidosDownloaded(List<PedidoModel> pedidoList)
    {
        hideProgress();

        pedidoModels.clear();
        pedidoModels.addAll(pedidoList);

        pedidoAdapter.notifyDataSetChanged();
    }

    @Override
    public void error()
    {
        hideProgress();
    }

    @Override
    public void unauth()
    {
        hideProgress();
    }

    private void downloadPedidos()
    {
        showProgress();
        UserModel model = new UserModel();
        model.setName(SessionManager.getInstance().getUser());

        PedidoPresenter presenter = new PedidoPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this
        );

        presenter.downloadPedidos(model);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout:
                promptLogout();
                return true;
            default:
                return onOptionsItemSelected(item);
        }
    }

}
