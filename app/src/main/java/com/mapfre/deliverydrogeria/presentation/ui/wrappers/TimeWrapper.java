/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.ui.wrappers;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mapfre.deliverydrogeria.R;
import com.mapfre.deliverydrogeria.domain.models.TrackingTime;

/**
 * Created by Bardur Thomsen on 15/09/2017.
 *
 * Class for wrapping TrackingTime for a ListView
 */

public class TimeWrapper
{
    private final View      row;
    private final Context   context;
    private ImageView       selectedView;
    private TextView        timeView;

    public TimeWrapper(Context context, View view)
    {
        this.context    = context;
        this.row        = view;
    }

    private ImageView getSelectedView(){
        if(selectedView == null){
            selectedView = row.findViewById(R.id.iv_selected);
        }

        return selectedView;
    }

    private TextView getTimeView(){
        if(timeView == null){
            timeView = row.findViewById(R.id.tv_time);
        }

        return timeView;
    }

    /**
     * Method to add model into ListView item
     * @param time - Model containing the parameters for item
     */
    public void populateFrom(final TrackingTime time)
    {
        if(time.isSelected()){
            getSelectedView().setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_radio_button_checked));
        }else {
            getSelectedView().setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_radio_button));
        }

        getTimeView().setText(time.getTimeString());

    }
}