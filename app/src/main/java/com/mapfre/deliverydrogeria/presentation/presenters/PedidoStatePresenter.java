/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.presentation.presenters;

import com.mapfre.deliverydrogeria.domain.models.PedidoStateModel;
import com.mapfre.deliverydrogeria.presentation.presenters.base.BasePresenter;
import com.mapfre.deliverydrogeria.presentation.ui.BaseView;

/**
 * Presenter to change state of Pedido.
 * This presenter calls PedidoStateInteractor
 *
 * @author Bardur Thomsen
 * @version 1.0 23/10/2017.
 */
public interface PedidoStatePresenter extends BasePresenter{

    interface View extends BaseView{
        void onStateChanged(String state);
        void onStateError(String state, String message);
        void onGeneralError();
    }

    /**
     * Method to change the state of a Pedido.
     * This method creates and startes a PedidoStateInteractor
     *
     * @param stateModel Parameter for changing the state of a Pedido
     */
    void changeState(PedidoStateModel stateModel);
}
