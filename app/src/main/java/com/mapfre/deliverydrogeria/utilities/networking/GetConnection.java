/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.utilities.networking;

import com.mapfre.deliverydrogeria.utilities.Constants;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by Bardur on 03/05/2017.
 *
 * Class for making a GET connection
 */

public class GetConnection extends Connection
{

    @Override
    public HttpURLConnection connect(String urlAddress) {
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection con= (HttpURLConnection) url.openConnection();

            //SET PROPS
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            con.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
            con.setReadTimeout(Constants.READ_TIME_OUT);
            con.setDoInput(true);

            return con;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public HttpURLConnection connectAuth(String urlAddress, String token) {
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            //SET PROPS
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            con.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
            con.setReadTimeout(Constants.READ_TIME_OUT);
            con.setDoInput(true);

            con.addRequestProperty("Authorization", "Bearer " + token);

            return con;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
