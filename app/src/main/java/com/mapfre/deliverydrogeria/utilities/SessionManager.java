/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Bardur Thomsen on 19/10/2017.
 *
 * Class for storing user session
 */

public class SessionManager {

    private final String PREFS_NAME = "mapfre.session";
    private final String USER_NAME = "username";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private static final SessionManager ourInstance = new SessionManager();

    public static SessionManager getInstance() {
        return ourInstance;
    }

    private SessionManager() {}

    public void init(Context context){

        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public String getUser(){
        return preferences.getString(USER_NAME, "");
    }

    public void saveUser(String user){
        editor.putString(USER_NAME,user);
        editor.commit();
    }

    public void destroySession(){
        editor.clear();
        editor.commit();
    }

    public boolean isLoggedIn(){

        boolean loggedIn = false;

        if(!getUser().isEmpty()){
            loggedIn = true;
        }

        return loggedIn;
    }

}
