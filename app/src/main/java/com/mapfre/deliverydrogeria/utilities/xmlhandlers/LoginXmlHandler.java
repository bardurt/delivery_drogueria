/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.utilities.xmlhandlers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by Bardur Thomsen on 21/10/2017.
 *
 * Class to extract server response when logging in
 */
public class LoginXmlHandler extends DefaultHandler {

    private String elementValue = null;
    private Boolean elementOn = false;

    private LoginResponse respnse;

    public LoginResponse getResponse(){
        return respnse;
    }
    /**
     * This will be called when the tags of the XML starts.
     **/
    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {


        if(localName.equalsIgnoreCase("EN_Usuario")){
            respnse = new LoginResponse();
        }

        elementOn = true;
    }

    /**
     * This will be called when the tags of the XML end.
     **/
    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {

        elementOn = false;

        /**
         * Sets the values after retrieving the values from the XML tags
         * */
        if (localName.equalsIgnoreCase("cueps")) {
            respnse.setCueps(elementValue);
        } else if (localName.equalsIgnoreCase("mensaje")){
            respnse.setMessage(elementValue);
        }

    }

    /**
     * This is called to get the tags value
     **/
    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {

        if (elementOn) {
            elementValue = new String(ch, start, length);
            elementOn = false;
        }

    }

    public class LoginResponse {
        private String cueps;
        private String message;

        public LoginResponse(){
            this.cueps = "";
            this.message = "";
        }

        public String getCueps() {
            return cueps;
        }

        public void setCueps(String cueps) {
            this.cueps = cueps;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}