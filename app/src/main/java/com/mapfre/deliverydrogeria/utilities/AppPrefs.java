/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.utilities;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * @author Bardur Thomsen
 *
 * Singleton class to store settings and parameters related to the
 * application.
 */
public class AppPrefs {

    private static final String PREFS_NAME = "mapfre.app.prefs";
    public static final String REMEMBER_ME = "remember_me";

    private SharedPreferences preferences;
    private SharedPreferences.Editor preferencesEdit;

    private static final AppPrefs ourInstance = new AppPrefs();

    public static AppPrefs getInstance() {
        return ourInstance;
    }

    private AppPrefs() {
    }

    public void init(Context context){
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        preferencesEdit = preferences.edit();
    }

    public void saveString(String field, String value){
        preferencesEdit.putString(field,value);
        preferencesEdit.commit();
    }

    public void saveBoolean(String field, boolean value){
        preferencesEdit.putBoolean(field, value);
        preferencesEdit.commit();
    }

    public String getString(String field){
        return preferences.getString(field, "");
    }

    public Boolean getBoolean(String field) {return preferences.getBoolean(field, false); }

}
