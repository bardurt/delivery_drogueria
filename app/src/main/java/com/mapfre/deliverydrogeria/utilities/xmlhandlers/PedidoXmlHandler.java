/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.utilities.xmlhandlers;

import com.mapfre.deliverydrogeria.domain.models.PedidoModel;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

/**
 * Created by Bardur Thomsen on 22/10/2017.
 */

public class PedidoXmlHandler extends DefaultHandler {

    private String elementValue = null;
    private Boolean elementOn = false;
    private final ArrayList<PedidoModel> pedidoModels = new ArrayList<>();
    private PedidoModel curentModel;

    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {

        elementOn = true;
    }

    /**
     * This will be called when the tags of the XML end.
     **/
    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {

        elementOn = false;

        if(localName.equalsIgnoreCase("CFNNCDRA")){
            curentModel = new PedidoModel();
            curentModel.setCFNNCDRA(elementValue);
        } else if(localName.equalsIgnoreCase("ESTADO")){
            curentModel.setESTADO(elementValue);
        } else if(localName.equalsIgnoreCase("FRQRMTO")) {
            curentModel.setFRQRMTO(elementValue);
        } else if(localName.equalsIgnoreCase("NOM_CLIENTE")) {
            curentModel.setCLIENTE(elementValue);
        } else if(localName.equalsIgnoreCase("NRQRMTO")){
            curentModel.setNRQRMTO(elementValue);
            pedidoModels.add(curentModel);
        }

    }

    /**
     * This is called to get the tags value
     **/
    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {

        if (elementOn) {
            elementValue = new String(ch, start, length);
            elementOn = false;
        }

    }

    public ArrayList<PedidoModel> getPedidoModels(){
        return pedidoModels;
    }
}
