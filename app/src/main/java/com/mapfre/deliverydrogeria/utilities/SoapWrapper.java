/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.utilities;

import com.mapfre.deliverydrogeria.domain.models.TrackingModel;

/**
 * Created by Bardur Thomsen on 21/10/2017.
 *
 * Class for creating SOAP bodies for http SOAP requests
 */

public class SoapWrapper {

    public static String wrapLogin(String username, String password)
    {

        String value =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" +
                    "<soapenv:Header/>" +
                    "<soapenv:Body>" +
                        "<tem:ListaUsuario>" +
                            "<tem:user>" + username + "</tem:user>" +
                            "<tem:password>" + password + "</tem:password>" +
                        "</tem:ListaUsuario>" +
                    "</soapenv:Body>" +
                "</soapenv:Envelope>";

        return value;
    }

    public static String wrapPedidosForUser(String user)
    {

        String value =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" +
                    "<soapenv:Header/>" +
                    "<soapenv:Body>" +
                        "<tem:ListaPedidosUsuario>" +
                            "<tem:user>" + user + "</tem:user>" +
                        "</tem:ListaPedidosUsuario>" +
                    "</soapenv:Body>" +
                "</soapenv:Envelope>";

        return value;
    }

    public static String wrapIniPedido(String srqrmnnto, String nrqrmnto)
    {

        String value =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" +
                    "<soapenv:Header/>" +
                    "<soapenv:Body>" +
                        "<tem:IniPedido>" +
                            "<tem:srqrmnnto>" + srqrmnnto + "</tem:srqrmnnto>" +
                            "<tem:nrqrmnto>" + nrqrmnto + "</tem:nrqrmnto>" +
                        "</tem:IniPedido>" +
                    "</soapenv:Body>" +
                "</soapenv:Envelope>";


        return value;
    }

    public static String wrapTrackingPedido(TrackingModel model)
    {
        String value =
                    "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" +
                            "<soapenv:Header/>" +
                            "<soapenv:Body>" +
                                "<tem:TrackingPedido>" +
                                    "<tem:nrqrmnto>" + model.getNrqrmnto() + "</tem:nrqrmnto>" +
                                    "<tem:longitud>" + model.getLongitude() + "</tem:longitud>" +
                                    "<tem:latitud>" + model.getLatitude() + "</tem:latitud>" +
                                    "<tem:cucrgstro>" + model.getCucrgstro() + "</tem:cucrgstro>" +
                                "</tem:TrackingPedido>" +
                            "</soapenv:Body>" +
                            "</soapenv:Envelope>";

        return value;
    }
}
