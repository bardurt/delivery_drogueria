/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Bardur Thomsen on 19/04/2017.
 */

public class AndroidNetworkChecker implements NetworkChecker
{

    private final ConnectivityManager   mConnectivityManager;
    private NetworkInfo                 mWifi;
    private NetworkInfo                 mData;

    public AndroidNetworkChecker(Context context)
    {
        mConnectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        updateConnectionState();
    }

    @Override
    public void refresh()
    {
      updateConnectionState();
    }

    @Override
    public boolean hasWifi()
    {

        boolean wifi;
        try {
            wifi = mWifi.isConnected();
        }catch (Exception e){
            wifi = false;
        }

        return wifi;
    }

    @Override
    public boolean hasMobileData()
    {
        boolean mobileData;
        try{
            mobileData = mData.isConnected();
        }catch (Exception e){
            mobileData = false;
        }

        return mobileData;
    }

    private void updateConnectionState()
    {

        getConnection();
    }

    private void getConnection()
    {
        mWifi = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        mData = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
    }
}
