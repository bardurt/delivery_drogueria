/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.utilities.networking;

import java.net.HttpURLConnection;

/**
 * Created by Bardur on 03/05/2017.
 *
 * Base class for network connections
 */

public abstract class Connection
{

    /**
     * Interface for setting up a connection
     *
     * @param urlAddress - URL to which to connect
     * @return - url connection
     */
    public abstract HttpURLConnection connect(String urlAddress);

    /**
     * Interface for setting up a connection with authorization
     *
     * @param urlAddress    - URL to which to connect
     * @param token         - Token which is used by the server
     * @return              - url connection
     */
    public abstract HttpURLConnection connectAuth(String urlAddress, String token);
}
