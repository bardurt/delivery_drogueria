/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.domain.models;

/**
 * Created by Bardur Thomsen on 07/09/2017.
 *
 * Class to which represents a Pedido.
 */

public class PedidoModel
{
    private String CFNNCDRA;
    private String ESTADO;
    private String FRQRMTO;
    private String CLIENTE;
    private String NRQRMTO;

    public PedidoModel(){
        this.CFNNCDRA = "";
        this.ESTADO   = "";
        this.FRQRMTO  = "";
        this.CLIENTE  = "";
        this.NRQRMTO  = "";
    }

    public String getCFNNCDRA() {
        return CFNNCDRA;
    }

    public void setCFNNCDRA(String CFNNCDRA) {
        this.CFNNCDRA = CFNNCDRA;
    }

    public String getESTADO() {
        return ESTADO;
    }

    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }

    public String getFRQRMTO() {
        return FRQRMTO;
    }

    public void setFRQRMTO(String FRQRMTO) {
        this.FRQRMTO = FRQRMTO;
    }

    public String getCLIENTE() {
        return CLIENTE;
    }

    public void setCLIENTE(String CLIENTE) {
        this.CLIENTE = CLIENTE;
    }

    public String getNRQRMTO() {
        return NRQRMTO;
    }

    public void setNRQRMTO(String NRQRMTO) {
        this.NRQRMTO = NRQRMTO;
    }
}
