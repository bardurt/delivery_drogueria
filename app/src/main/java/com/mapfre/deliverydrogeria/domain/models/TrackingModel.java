/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.domain.models;

/**
 * Created by Bardur Thomsen on 12/09/2017.
 *
 * Class for tracking a Pedido.
 * The class contains the necessary parameters
 * which are used for tracking
 */

public class TrackingModel
{

    private double longitude;
    private double latitude;
    private double altitude;
    private String nrqrmnto;
    private String cucrgstro;
    private String time;

    public TrackingModel(){
        this.longitude  = 0.0d;
        this.latitude   = 0.0d;
        this.altitude   = 0.0d;
        this.nrqrmnto   = "";
        this.cucrgstro  = "";
        this.time       = "";
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public String getNrqrmnto() {
        return nrqrmnto;
    }

    public void setNrqrmnto(String nrqrmnto) {
        this.nrqrmnto = nrqrmnto;
    }

    public String getCucrgstro() {
        return cucrgstro;
    }

    public void setCucrgstro(String cucrgstro) {
        this.cucrgstro = cucrgstro;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
