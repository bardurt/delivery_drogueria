/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.domain.interactors.base;


import com.mapfre.deliverydrogeria.domain.executor.Executor;
import com.mapfre.deliverydrogeria.domain.executor.MainThread;

public abstract class AbstractInteractor implements Interactor
{

    protected final MainThread  mMainThread;
    private final Executor      mThreadExecutor;
    private volatile boolean    mIsRunning;

    public AbstractInteractor(Executor threadExecutor, MainThread mainThread)
    {
        mThreadExecutor = threadExecutor;
        mMainThread = mainThread;
    }

    public abstract void run();

    public boolean isRunning()
    {
        return mIsRunning;
    }

    public void onFinished()
    {
        mIsRunning = false;
    }

    public void execute()
    {
        // mark this interactor as running
        this.mIsRunning = true;

        // start running this interactor in a background thread
        mThreadExecutor.execute(this);
    }

}
