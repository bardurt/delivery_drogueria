/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.domain.interactors;

import com.mapfre.deliverydrogeria.domain.interactors.base.Interactor;
import com.mapfre.deliverydrogeria.domain.models.TrackingModel;

/**
 * Created by Bardur Thomsen on 12/09/2017.
 *
 * Interface for sending tracking updates to webserver
 */

public interface TrackingInteractor extends Interactor
{
    interface Callback{
        void success(TrackingModel model);
        void error();
        void unauth();
    }
}
