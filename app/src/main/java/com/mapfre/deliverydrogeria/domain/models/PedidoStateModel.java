/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.domain.models;

/**
 * Class for change state of a Pedido
 *
 * @author Bardur Thomsen
 * @version 1.0 23/10/2017.
 */
public class PedidoStateModel
{
    private String srqrmnnto;
    private String nrqrmnto;

    public String getSrqrmnnto() {
        return srqrmnnto;
    }

    public void setSrqrmnnto(String srqrmnnto) {
        this.srqrmnnto = srqrmnnto;
    }

    public String getNrqrmnto() {
        return nrqrmnto;
    }

    public void setNrqrmnto(String nrqrmnto) {
        this.nrqrmnto = nrqrmnto;
    }
}
