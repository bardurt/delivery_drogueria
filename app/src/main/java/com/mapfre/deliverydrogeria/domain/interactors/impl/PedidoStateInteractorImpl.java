/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.domain.interactors.impl;

import com.mapfre.deliverydrogeria.domain.executor.Executor;
import com.mapfre.deliverydrogeria.domain.executor.MainThread;
import com.mapfre.deliverydrogeria.domain.interactors.PedidoStateInteractor;
import com.mapfre.deliverydrogeria.domain.interactors.base.AbstractInteractor;
import com.mapfre.deliverydrogeria.domain.models.PedidoStateModel;
import com.mapfre.deliverydrogeria.utilities.AppFields;
import com.mapfre.deliverydrogeria.utilities.SoapWrapper;
import com.mapfre.deliverydrogeria.utilities.networking.PostConnection;
import com.mapfre.deliverydrogeria.utilities.xmlhandlers.PedidoStateHandler;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 *
 * @author Bardur Thomsen
 * @version 1.0 23/10/2017.
 */
public class PedidoStateInteractorImpl extends AbstractInteractor implements PedidoStateInteractor{

    private final Callback callback;
    private final PedidoStateModel stateModel;

    public PedidoStateInteractorImpl(Executor threadExecutor, MainThread mainThread,
                                     Callback callback, PedidoStateModel stateModel) {
        super(threadExecutor, mainThread);

        this.callback = callback;
        this.stateModel = stateModel;
    }

    @Override
    public void run() {

        changePedidoState();
    }

    private void changePedidoState(){

        HttpURLConnection con =  new PostConnection().connect(AppFields.Networking.WEB_SERVICE_WDSL);

        if(con != null) {

            try {

                // SOAP 1.1
                con.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

                // What service method to call
                con.setRequestProperty("SOAPAction", "http://tempuri.org/IDrogueriaPedido/IniPedido");

                OutputStream os=con.getOutputStream();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));

                // Create request body
                bw.write(SoapWrapper.wrapIniPedido(stateModel.getSrqrmnnto(), stateModel.getNrqrmnto()));

                bw.flush();
                bw.close();
                os.close();

                final int responseCode = con.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK) {

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder response = new StringBuilder();

                    String line;

                    while ((line = br.readLine()) != null) {
                        response.append(line);
                    }

                    br.close();

                    try {

                        String error = response.toString();

                        SAXParserFactory saxPF = SAXParserFactory.newInstance();
                        SAXParser saxP = saxPF.newSAXParser();
                        XMLReader xmlR = saxP.getXMLReader();

                        InputSource inputSource = new InputSource();
                        inputSource.setEncoding("UTF-8");
                        inputSource.setCharacterStream(new StringReader(error));

                        final PedidoStateHandler xmlHandler = new PedidoStateHandler();
                        xmlR.setContentHandler(xmlHandler);
                        xmlR.parse(inputSource);

                        boolean ok = xmlHandler.getResult().equalsIgnoreCase("El estado del pedido se actualizó correctamente.");

                        if(ok) {

                            mMainThread.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onStateChanged(stateModel.getSrqrmnnto());
                                }
                            });
                        } else {

                            mMainThread.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onStateError(stateModel.getSrqrmnnto(), xmlHandler.getResult());
                                }
                            });
                        }


                    } catch (Exception e) {
                        mMainThread.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onGeneralError();
                            }
                        });
                    }
                } else {
                    mMainThread.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onGeneralError();
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
                mMainThread.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onGeneralError();
                    }
                });
            }
        } else {
            mMainThread.post(new Runnable() {
                @Override
                public void run() {
                    callback.onGeneralError();
                }
            });
        }


    }
}
