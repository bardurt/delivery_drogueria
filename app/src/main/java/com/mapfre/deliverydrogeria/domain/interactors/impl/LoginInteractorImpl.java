/*
 * Copyright (c) 2017.  Bardur Thomsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mapfre.deliverydrogeria.domain.interactors.impl;

import com.mapfre.deliverydrogeria.domain.executor.Executor;
import com.mapfre.deliverydrogeria.domain.executor.MainThread;
import com.mapfre.deliverydrogeria.domain.interactors.LoginInteractor;
import com.mapfre.deliverydrogeria.domain.interactors.base.AbstractInteractor;
import com.mapfre.deliverydrogeria.domain.models.LoginModel;
import com.mapfre.deliverydrogeria.utilities.AppFields;
import com.mapfre.deliverydrogeria.utilities.SoapWrapper;
import com.mapfre.deliverydrogeria.utilities.networking.PostConnection;
import com.mapfre.deliverydrogeria.utilities.xmlhandlers.LoginXmlHandler;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by Bardur Thomsen on 07/09/2017.
 */

public class LoginInteractorImpl extends AbstractInteractor implements LoginInteractor
{

    private final Callback    callback;
    private final LoginModel  model;

    public LoginInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback callback, LoginModel model)
    {
        super(threadExecutor, mainThread);

        this.callback   = callback;
        this.model      = model;
    }

    @Override
    public void run()
    {
        login();
    }

    private void login()
    {

        HttpURLConnection con =  new PostConnection().connect(AppFields.Networking.WEB_SERVICE_WDSL);

        if(con != null) {

            try {

                // SOAP 1.1
                con.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

                // What service method to call
                con.setRequestProperty("SOAPAction", "http://tempuri.org/IDrogueriaPedido/ListaUsuario");

                OutputStream os=con.getOutputStream();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));

                // Create request body
                bw.write(SoapWrapper.wrapLogin(model.getUserName(), model.getPassword()));

                bw.flush();
                bw.close();
                os.close();

                final int responseCode = con.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK) {

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder response = new StringBuilder();

                    String line;

                    while ((line = br.readLine()) != null) {
                        response.append(line);
                    }

                    br.close();

                    try {

                        String error = response.toString();

                        SAXParserFactory saxPF = SAXParserFactory.newInstance();
                        SAXParser saxP = saxPF.newSAXParser();
                        XMLReader xmlR = saxP.getXMLReader();

                        InputSource inputSource = new InputSource();
                        inputSource.setEncoding("UTF-8");
                        inputSource.setCharacterStream(new StringReader(error));

                        final LoginXmlHandler xmlHandler = new LoginXmlHandler();
                        xmlR.setContentHandler(xmlHandler);
                        xmlR.parse(inputSource);


                        if(xmlHandler.getResponse().getCueps().equalsIgnoreCase("0")){
                            mMainThread.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.loginFail(xmlHandler.getResponse().getMessage());
                                }
                            });
                        } else {
                            mMainThread.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.loginSuccess(xmlHandler.getResponse().getCueps());
                                }
                            });
                        }

                    } catch (Exception e) {
                        mMainThread.post(new Runnable() {
                            @Override
                            public void run() {
                               callback.error();
                            }
                        });
                    }
                } else if(responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    mMainThread.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.unauth();
                        }
                    });
                } else {
                    mMainThread.post(new Runnable() {
                        @Override
                        public void run() {
                           callback.error();
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
                callback.error();
            }
        } else {
            mMainThread.post(new Runnable() {
                @Override
                public void run() {
                    callback.error();
                }
            });
        }
    }
}
